import { getItemsByBasketId } from "../postgress/item";
import { getOfferByProductId } from "../postgress/offer";

const getItemsByBasketIdService = async (data) => {
  const items = await getItemsByBasketId(data);
  if (items.length) {
    for (const item of items) {
      if (item.product_has_offer) {
        const productId = item.product_id;
        const offer = await getOfferByProductId(productId);
      }
    }
  }
  return items;
};

export { getItemsByBasketIdService };
