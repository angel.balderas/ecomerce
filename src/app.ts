import express from "express";

const { PORT } = process.env;
const app = express();
const port = PORT || "3000";

import cookieParser from "cookie-parser";
import api from "./api";

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use("/api/v1", api);

app.listen(port, () => {
  return console.log(`server is listening on ${port}`);
});
