import { Router } from "express";
import {
  createProduct,
  getProductById,
  deleteProduct,
  getProducts,
  updateProduct,
} from "../postgress/product";
const router = Router();
/* GET home page. */
router.get("/", async (req, res, next) => {
  const result = await getProducts();
  res.send(result);
});
router.get("/productById", async (req, res, next) => {
  const data = req.body;
  const result = await getProductById(data);
  res.send(result);
});
router.post("/product", async (req, res, next) => {
  const data = req.body;
  const result = await createProduct(data);
  res.send(result);
});
router.delete("/product", async (req, res, next) => {
  const data = req.body;
  const result = await deleteProduct(data);
  res.send(result);
});
router.put("/product", async (req, res, next) => {
  const data = req.body;
  const result = await updateProduct(data);
  res.send(result);
});
export default router;
