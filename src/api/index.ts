import { Router } from "express";
const router = Router();

import product from "./product";
import basket from "./basket";
import customer from "./customer";
import item from "./items";

router.use("/products", product);
router.use("/customers", customer);
router.use("/baskets", basket);
router.use("/items", item);

export default router;
