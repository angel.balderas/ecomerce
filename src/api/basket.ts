import { Router } from "express";
import {
  createBasket,
  getBasketById,
  deleteBasket,
  getBaskets,
  updateBasket,
  getBasketByCustomerId,
} from "../postgress/basket";
const router = Router();
router.get("/", async (req, res, next) => {
  const result = await getBaskets();
  res.send(result);
});
router.get("/basketById", async (req, res, next) => {
  const data = req.body;
  const result = await getBasketById(data);
  res.send(result);
});
router.get("/basketByCustomerId", async (req, res, next) => {
  const data = req.body;
  const result = await getBasketByCustomerId(data);
  res.send(result);
});
router.post("/basket", async (req, res, next) => {
  const data = req.body;
  const result = await createBasket(data);
  res.send(result);
});
router.delete("/basket", async (req, res, next) => {
  const data = req.body;
  const result = await deleteBasket(data);
  res.send(result);
});
router.put("/basket", async (req, res, next) => {
  const data = req.body;
  const result = await updateBasket(data);
  res.send(result);
});
export default router;
