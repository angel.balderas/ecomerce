import { Router } from "express";
import {
  createCustomer,
  getCustomerById,
  deleteCustomer,
  getCustomers,
  updateCustomer,
} from "../postgress/customer";
const router = Router();
/* GET home page. */
router.get("/", async (req, res, next) => {
  const result = await getCustomers();
  res.send(result);
});
router.get("/customerById", async (req, res, next) => {
  const data = req.body;
  const result = await getCustomerById(data);
  res.send(result);
});
router.post("/customer", async (req, res, next) => {
  const data = req.body;
  const result = await createCustomer(data);
  res.send(result);
});
router.delete("/customer", async (req, res, next) => {
  const data = req.body;
  const result = await deleteCustomer(data);
  res.send(result);
});
router.put("/customer", async (req, res, next) => {
  const data = req.body;
  const result = await updateCustomer(data);
  res.send(result);
});
export default router;
