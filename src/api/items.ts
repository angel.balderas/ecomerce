import { Router } from "express";
import {
  createItem,
  getItemById,
  deleteItem,
  getItems,
  updateItem,
} from "../postgress/item";

import { getItemsByBasketIdService } from "../services/items";

const router = Router();
/* GET home page. */
router.get("/", async (req, res, next) => {
  const result = await getItems();
  res.send(result);
});
router.get("/itemById", async (req, res, next) => {
  const data = req.body;
  const result = await getItemById(data);
  res.send(result);
});
router.get("/byBasketId", async (req, res, next) => {
  const data = req.body;
  const result = await getItemsByBasketIdService(data);
  res.send(result);
});
router.post("/item", async (req, res, next) => {
  const data = req.body;
  const result = await createItem(data);
  res.send(result);
});
router.delete("/item", async (req, res, next) => {
  const data = req.body;
  const result = await deleteItem(data);
  res.send(result);
});
router.put("/item", async (req, res, next) => {
  const data = req.body;
  const result = await updateItem(data);
  res.send(result);
});
export default router;
