import pool from "./config";

const getItems = async () => {
  const { rows } = await pool.query("SELECT * FROM items ORDER BY item_id ASC");
  console.log(`pg rows ${JSON.stringify(rows)}`);
  return rows;
};

const getItemById = async (data) => {
  const { itemId } = data;
  const { rows } = await pool.query("SELECT * FROM items WHERE item_id = $1", [
    itemId,
  ]);
  return rows;
};

const getItemsByBasketId = async (data) => {
  const { basketId } = data;
  const { rows } = await pool.query(
    "SELECT items.item_id,items.basket_id,items.product_id, products.product_code, products.product_name, products.product_price, product_has_offer \
      FROM items INNER JOIN products ON items.product_id = products.product_id \
      INNER JOIN baskets ON items.basket_id = baskets.basket_id \
      WHERE baskets.basket_id = $1",
    [basketId]
  );
  return rows;
};

const createItem = async (data) => {
  const { basketId, productId } = data;
  const { err } = pool.query(
    "INSERT INTO items (basket_id, productId) VALUES ($1,$2)",
    [basketId, productId]
  );
  return err;
};

const updateItem = async (data) => {
  const { basketId, productId, itemId } = data;
  const { rows } = await pool.query(
    "UPDATE items SET basket_id=$1, productId=$2 WHERE item_id=$3",
    [basketId, productId, itemId]
  );
  console.log(`rows ${JSON.stringify(rows)}`);
  return rows;
};

const deleteItem = async (data) => {
  const { itemId } = data;
  const { rows } = await pool.query("DELETE FROM items WHERE item_id = $1", [
    itemId,
  ]);
  return rows;
};

export {
  getItems,
  getItemById,
  getItemsByBasketId,
  createItem,
  updateItem,
  deleteItem,
};
