import pool from "./config";

const getBaskets = async () => {
  const { rows } = await pool.query(
    "SELECT * FROM baskets ORDER BY basket_id ASC"
  );
  console.log(`pg rows ${JSON.stringify(rows)}`);
  return rows;
};

const getBasketById = async (data) => {
  const { basketId } = data;
  const { rows } = await pool.query(
    "SELECT * FROM baskets WHERE basket_id = $1",
    [basketId]
  );
  return rows;
};

const getBasketByCustomerId = async (data) => {
  const { customerId } = data;
  const { rows } = await pool.query(
    "SELECT * FROM baskets WHERE customer_id = $1",
    [customerId]
  );
  return rows;
};

const createBasket = async (data) => {
  const { customerId } = data;
  const { err } = pool.query("INSERT INTO baskets (customer_id) VALUES ($1)", [
    customerId,
  ]);
  return err;
};

const updateBasket = async (data) => {
  const { customerId, basketId } = data;
  const { rows } = await pool.query(
    "UPDATE baskets SET customer_id=$1 WHERE basket_id = $2",
    [customerId, basketId]
  );
  return rows;
};

const deleteBasket = async (data) => {
  const { basketId } = data;
  const { rows } = await pool.query(
    "DELETE FROM baskets WHERE basket_id = $1",
    [basketId]
  );
  return rows;
};

export {
  getBaskets,
  getBasketById,
  getBasketByCustomerId,
  createBasket,
  updateBasket,
  deleteBasket,
};
