import pool from "./config";

const getProducts = async () => {
  const { rows } = await pool.query(
    "SELECT * FROM products ORDER BY product_id ASC"
  );
  console.log(`pg rows ${JSON.stringify(rows)}`);
  return rows;
};

const getProductById = async (data) => {
  const { productId } = data;
  const { rows } = await pool.query(
    "SELECT * FROM products WHERE product_id = $1",
    [productId]
  );
  return rows;
};

const createProduct = async (data) => {
  const { productCode, productName, productPrice, productHasOffer } = data;
  const { err } = pool.query(
    "INSERT INTO products (product_code, product_name, product_price, product_has_offer=$4) VALUES ($1, $2, $3, $4)",
    [productCode, productName, productPrice, productHasOffer]
  );
  return err;
};

const updateProduct = async (data) => {
  const { productId, productCode, productName, productPrice, productHasOffer } =
    data;
  const { rows } = await pool.query(
    "UPDATE products SET product_code=$1, product_name=$2, product_price=$3, product_has_offer=$4 WHERE product_id = $5",
    [productCode, productName, productPrice, productHasOffer, productId]
  );
  console.log(`rows ${JSON.stringify(rows)}`);
  return rows;
};

const deleteProduct = async (data) => {
  const { productId } = data;
  const { err } = await pool.query(
    "DELETE FROM products WHERE product_id = $1",
    [productId]
  );
  return err;
};

export {
  getProducts,
  getProductById,
  createProduct,
  updateProduct,
  deleteProduct,
};
