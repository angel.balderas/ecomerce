import pool from "./config";

const getCustomers = async () => {
  const { rows } = await pool.query(
    "SELECT * FROM customers ORDER BY customer_id ASC"
  );
  console.log(`pg rows ${JSON.stringify(rows)}`);
  return rows;
};

const getCustomerById = async (data) => {
  const { customerId } = data;
  const { rows } = await pool.query(
    "SELECT * FROM customers WHERE customer_id = $1",
    [customerId]
  );
  return rows;
};

const createCustomer = async (data) => {
  const { customerName, customerPhone } = data;
  const { err } = pool.query(
    "INSERT INTO customers (customer_name, customer_phone) VALUES ($1, $2)",
    [customerName, customerPhone]
  );
  return err;
};

const updateCustomer = async (data) => {
  const { customerName, customerPhone, customerId } = data;
  const { rows } = await pool.query(
    "UPDATE customers SET customer_name=$1, customer_phone=$2, WHERE customer_id = $3",
    [customerName, customerPhone, customerId]
  );
  return rows;
};

const deleteCustomer = async (data) => {
  const { customerId } = data;
  const { err } = await pool.query(
    "DELETE FROM customers WHERE customer_id = $1",
    [customerId]
  );
  return err;
};

export {
  getCustomers,
  getCustomerById,
  createCustomer,
  updateCustomer,
  deleteCustomer,
};
