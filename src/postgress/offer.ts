import pool from "./config";

const getOffers = async () => {
  const { rows } = await pool.query(
    "SELECT * FROM offers ORDER BY offer_id ASC"
  );
  return rows;
};

const getOfferById = async (data) => {
  const { offerId } = data;
  const { rows } = await pool.query(
    "SELECT * FROM offers WHERE offer_id = $1",
    [offerId]
  );
  return rows;
};

const getOfferByProductId = async (productId) => {
  // const { productId } = data;
  const { rows } = await pool.query(
    "SELECT * FROM offers WHERE product_id = $1",
    [productId]
  );
  return rows;
};

const createOffer = async (data) => {
  const { offerCode, offerName, productId, offerDiscount } = data;
  const { rows } = pool.query(
    "INSERT INTO offers (offer_code, offer_name, product_id, offer_discount) VALUES ($1, $2, $3, $4)",
    [offerCode, offerName, productId, offerDiscount]
  );
  return rows;
};

const updateOffer = async (data) => {
  const { offerCode, offerName, productId, offerDiscount, offer_id } = data;
  const { rows } = await pool.query(
    "UPDATE offers SET offer_code=$1, offer_name=$2, product_id=$3, offer_discount=$4  WHERE id = $5",
    [offerCode, offerName, productId, offerDiscount, offer_id]
  );
  return rows;
};

const deleteOffer = async (data) => {
  const { offerId } = data;
  const { rows } = await pool.query("DELETE FROM offers WHERE offer_id = $1", [
    offerId,
  ]);
  return rows;
};

export {
  getOffers,
  getOfferById,
  createOffer,
  updateOffer,
  deleteOffer,
  getOfferByProductId,
};
